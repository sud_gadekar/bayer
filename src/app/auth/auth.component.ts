import { Component, OnInit,ChangeDetectorRef,AfterViewInit,HostListener,Input, Inject,ViewChild} from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit,AfterViewInit {

  resCom =false;

constructor(
  private router: Router,
)
{
  
}

ngOnInit()
{
  this.resCom=true;
}

ngAfterViewInit()
{
  // $('#loginModal').on('hidden.bs.modal', function() {
  //   this.close.bind();
  // });
}

openLoginModal()
{
  $('#loginModal').modal('show');
}

close()
{
  this.router.navigateByUrl('/')
}

}
