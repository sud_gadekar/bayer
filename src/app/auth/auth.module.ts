import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { ToastrModule } from 'ngx-toastr';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { RecaptchaModule, RECAPTCHA_SETTINGS, RecaptchaSettings } from 'ng-recaptcha';
import { RecaptchaFormsModule} from 'ng-recaptcha';
import { NgxMatIntlTelInputModule } from 'ngx-mat-intl-tel-input';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatIconModule} from '@angular/material/icon';
import { ForgotPwdComponent } from './forgot-pwd/forgot-pwd.component';
import {MatCheckboxModule} from '@angular/material/checkbox';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
				path: '',
				component: LoginComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path : 'forgot-pwd',
        component : ForgotPwdComponent
      },
   
    ]
  }
]

@NgModule({
  declarations: [AuthComponent, LoginComponent,ForgotPwdComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    MatButtonModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    MatFormFieldModule,
    MatInputModule,
    NgxMatIntlTelInputModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatCheckboxModule
  ]
})
export class AuthModule { }
