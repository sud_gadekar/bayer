import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DataService } from 'src/app/shared-services/data.service';

@Component({
  selector: 'app-forgot-pwd',
  templateUrl: './forgot-pwd.component.html',
  styleUrls: ['./forgot-pwd.component.css']
})
export class ForgotPwdComponent implements OnInit {

  Attendee_Forgot_Password_Form:FormGroup;
  rescom = false;
  
  constructor(private FB: FormBuilder,
    private service: DataService,
    private router: Router,
    private toastr: ToastrService,) { }

  ngOnInit(): void {
    this.Attendee_Forgot_Password_Form = this.FB.group({
      forgotEmail: ['', [Validators.required,Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
  });
  }
  
  isControlHasError(controlName: string, validationType: string): boolean {
    let control = this.Attendee_Forgot_Password_Form.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
  
  forgotMailSend() {
   this.rescom =true;
    const email = this.Attendee_Forgot_Password_Form.controls['forgotEmail'].value;
    this.service.postRecord('passwordreset', {
        'email': email
    }).subscribe(res => {
        if (res.Status) {
            this.toastr.success(res.Msg, 'Success',{timeOut: 7000});
            this.router.navigate(['/'])
            this.rescom=false;
        } else 
        {
           this.toastr.error(res.Msg, 'Error', {timeOut: 3000});
            this.rescom = false;
        }
    });
}
}
