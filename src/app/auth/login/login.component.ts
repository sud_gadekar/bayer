import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../shared-services/data.service';
import { ToastrService } from 'ngx-toastr';
import { DOCUMENT } from '@angular/common';

declare var $;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  url = "login";
  rescom=false;

  constructor(private fb: FormBuilder, private ref: ChangeDetectorRef, private router: Router,
    private apiService : DataService, private toastr:ToastrService,
    @Inject(DOCUMENT) private document: Document) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      password : ['',[Validators.required]],
      terms : ['',[Validators.required]],
      privacy : ['',[Validators.required]],
    });

  }

  ngOnInit()
  {
    if(localStorage.getItem('email') && localStorage.getItem('password'))
    {
      this.loginForm.patchValue({
        email:localStorage.getItem('email'),
      });
    }
  }
  checkBox(event,type)
  {
    if ( !event.target.checked ) {
      if(type == 'privacy'){
        this.loginForm.controls['privacy'].setValue('');
      }
      if(type == 'terms'){
        this.loginForm.controls['terms'].setValue('');
      }
 }
  }
  
  isControlHasError(controlName: string, validationType: string): boolean {
    let control = this.loginForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  

  onSubmit() {
    this.rescom=true;
    if (this.loginForm.valid) {
      this.apiService.login(this.url, this.loginForm.value).subscribe(res => {
        if (res)
        {
          if(res.Status)
          {
            localStorage.setItem('accessToken', res.access_token);
            this.apiService.getProfile().subscribe(profile_res => {
              // console.log(res);
              if (profile_res.type) {
                  if (profile_res.type == 'attendee') {
                      localStorage.setItem('role', profile_res.type);
                      localStorage.setItem('language', profile_res.language);
                      //this.toastr.success('Login Successful', 'Success');
                      this.document.location.href = 'https://www.forschungswerkstattnetzhaut2021.com/virtual-expo/attendee/lobby';
                  } 
              }
          },error => {
            this.rescom = false;
            this.toastr.error('Server problem', 'Error', {timeOut: 3000,});
            this.ref.markForCheck();
         });
          this.rescom=false;
          }
          else {
            this.toastr.error('Login Failed', 'Error', {
              timeOut: 3000,
              progressBar: true,
              closeButton: true
            });
          }
        } 
        else {
          this.toastr.error('Login Failed', 'Error', {
            timeOut: 3000,
            progressBar: true,
            closeButton: true
          });
        }
        this.rescom=false;
      },error => {
        if(error.status == 401)
        {
          this.toastr.error('Invalid Username and password', 'Error', {timeOut: 3000,});
        }
        else
        {
          this.toastr.error('Server problem', 'Error', {timeOut: 3000,});
        }
        this.rescom = false;
        this.ref.markForCheck();
     });
    }
  }
  
}
