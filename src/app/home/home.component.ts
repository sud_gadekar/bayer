import { Component, OnInit,ChangeDetectorRef,AfterViewInit,HostListener,Input, Inject,ViewChild} from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl, } from '@angular/forms';
import { Router,  ActivatedRoute } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { DataService } from '../shared-services/data.service';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';


import {VERSION } from '@angular/core';

declare var $: any;

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    userToken;
  Attendee_Change_Password_Form : FormGroup;
  rescom = false;

  constructor(private FB: FormBuilder,
    private service: DataService,
    private router: Router,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.Attendee_Change_Password_Form = this.FB.group({
      password: ['',Validators.compose([Validators.required,])],
      confirm_password: ['',Validators.compose([Validators.required])]
  }, 
  {
      validator: this.mustMatch
  });
    
    if (this.activatedRoute.snapshot.queryParams['token']) {
      this.activatedRoute.queryParams.subscribe(params => {
          const userId = params['token'];
          if (userId) {
              this.userToken = userId;
          }
      });
  }
}

isControlHasError(controlName: string, validationType: string): boolean {
  let control = this.Attendee_Change_Password_Form.controls[controlName];
  if (!control) {
    return false;
  }

  const result = control.hasError(validationType) && (control.dirty || control.touched);
  return result;
}

mustMatch(group: FormGroup) {
  let passControl = group.get('password');
  let confirmPassControl = group.get('confirm_password');
  if (confirmPassControl.value) {
      if (passControl.value === confirmPassControl.value) {
          if (confirmPassControl.hasError('notSame')) {
              delete confirmPassControl.errors['notSame'];
              confirmPassControl.updateValueAndValidity();
          }
          return null;
      } else {
          confirmPassControl.setErrors({
              notSame: true
          });
          return {
              notSame: true
          };
      }
  }
}

changePassword() {
  this.rescom = true;
  console.log(this.userToken)
  const postData = {
      'password': this.Attendee_Change_Password_Form.controls['password'].value,
      'token': this.userToken
  }
  console.log(postData);

  this.service.postRecord('passwordchange', postData).subscribe(res => {
      if (res.Status) {
          this.toastr.success(res.Msg, 'Success',
          {
              timeOut: 3000,
              progressBar: true,
              closeButton: true
          });
          this.router.navigate(['/']);
      } else {
          this.toastr.error(res.Msg, 'Error',
          {
              timeOut: 3000,
              progressBar: true,
              closeButton: true
          });
      }
      this.rescom = false;
  });
}
}